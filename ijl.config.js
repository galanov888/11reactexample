const pkg = require('./package')

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`
        }
    },
    navigations: {
        '011reactrouter.main': '/011reactrouter',
        '011reactrouter.home': '/',
        '011reactrouter.about': '/about',
        '011reactrouter.items': '/item',
        '011reactrouter.item': '/item/:itemId',
    },
    features: {
        '011reactrouter': {
            // add your features here in the format [featureName]: { value: string }
        },
    },
    config: {
        key: 'value'
    }
}
