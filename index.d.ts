import { theme } from "./src/theme"

declare module '@emotion/react'{
    //1 вариант
    // export interface Theme{
    //    palette: typeof theme.palette
    // }
    //2 вариант
    type MyTheme = typeof theme
    export interface Theme extends MyTheme{
        
    }
}