import React from 'react';
import { Link, generatePath } from 'react-router-dom';
import { getNavigationsValue } from '@ijl/cli';

const Item = (props) =>{
    return <>
    <Link to={generatePath(getNavigationsValue(`011reactrouter.item`), {itemId: props.name})} replace >
    <h4>Item {props.name}</h4>
    </Link>
    </>
}

export default Item