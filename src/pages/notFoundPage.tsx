import styled from "@emotion/styled";
import { Controls, Player } from "@lottiefiles/react-lottie-player";
import React from "react";
import { NotFoundAnimation } from "../__data__/animations";

const StyledPlayer = styled(Player)`
  width: 300px;
  height: 300px;
`;

const NotFoundPage = () => {
  return (
    <>
      <StyledPlayer
        autoplay
        loop
        src={NotFoundAnimation}
       
      ></StyledPlayer>
    </>
  );
};

export default NotFoundPage;
