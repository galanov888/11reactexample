import React from 'react';
import { useParams } from 'react-router-dom';

const ItemPage = () =>{
    let {itemId} = useParams();

    if(!['First', 'Second'].includes(itemId))
    {
        throw new Error(
            'not found'
        )
    }

    return <>
    <h1>Item {itemId}</h1>
    </>
}

export default ItemPage