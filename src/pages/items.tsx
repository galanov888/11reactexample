import React from 'react';
import Item from '../components/item/item';

const ItemsPage = () =>{
    return <>
    <h1>Items</h1>
    <Item name = {'First'} />
    <Item name = {'Second'} />
    <Item name = {'Third'} />
    <Item name = {'Fourth'} />
    </>
}

export default ItemsPage