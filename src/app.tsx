import React from 'react';
import { createBrowserRouter, RouterProvider, Outlet } from 'react-router-dom';
import AboutPage from './pages/about';
import ItemPage from './pages/item';
import ItemsPage from './pages/items';
import Layout from './pages/layout';
import NotFoundPage from './pages/notFoundPage';
import { getNavigationsValue } from '@ijl/cli';
import { ThemeProvider } from '@emotion/react';
import { theme, theme2 } from './theme';

const router = createBrowserRouter([
    {
      path: getNavigationsValue('011reactrouter.home'),
      element: <Layout/>,
      //<><h1>Root router</h1><Outlet/></>, // Outlet - нужен для отображения children путей
      errorElement: <NotFoundPage/>,
      children: [
        {
            path: getNavigationsValue('011reactrouter.about'),
            element: <AboutPage/>
        },
        {
            path: getNavigationsValue( '011reactrouter.items'),
            element: <ItemsPage/>
        },
        {
            path: getNavigationsValue('011reactrouter.item'),
            element: <ItemPage/>
        }
      ],
    },
    // {
    //     path: "/about",
    //     element: <h1>About</h1>
    // }
    ],{basename: getNavigationsValue('011reactrouter.main')}); // изменили данные, чтобы они брались из конфигурационного файла
  //],{basename: '/011reactrouter'}); //указываем имя приложения для роутинга, чтобы при дефолтных настройках он всегда его видел

const App = () => {
    return(
        <ThemeProvider theme={theme2}>
            <RouterProvider router ={router}/>
        </ThemeProvider>
       
    )
}

export default App;

