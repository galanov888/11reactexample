import React from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import { getNavigationsValue } from '@ijl/cli';
import { StyledHeader, StyledNavLink } from './header.style';

const Header:React.FC = () =>{

    const navigate = useNavigate();

    return (
        <StyledHeader shadow>
            <span onClick={()=>navigate(getNavigationsValue('011reactrouter.home'))}>Logo</span>
            <div>
            <StyledNavLink to={getNavigationsValue('011reactrouter.about')}>About</StyledNavLink>
            <StyledNavLink to={getNavigationsValue( '011reactrouter.items')}>Items</StyledNavLink>
            </div>
        </StyledHeader>
    )
}

export default Header