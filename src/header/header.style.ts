import styled from "@emotion/styled";
import { NavLink } from "react-router-dom";
import { theme } from "../theme";

export const StyledHeader = styled.header<{shadow:boolean}>`
  height: 60px;
  background: ${(props) => props.theme.palette.primary.main};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  box-shadow: ${(props)=>props.shadow ? '2px 2px 2px 2px rgba(0 , 0,0,.3)': 'none'};
  col
`;

export const StyledNavLink = styled(NavLink)`
  margin: 0 10px;
`;
