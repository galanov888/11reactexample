export const theme ={
    palette:{
        primary:{
            main: '#1976d2',
            light: '#42a5f5',
            dark: '#1565c0'
        }
    },
    spacings:{
        sm: '2px',
        md: '8px',
        lg: '16px'
    }
}

export const theme2 ={
    palette:{
        primary:{
            main: '#7c8692',
            light: '#42a5f5',
            dark: '#1565c0'
        }
    },
    spacings:{
        sm: '2px',
        md: '8px',
        lg: '16px'
    }
}